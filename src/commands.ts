import { ResourceType } from './enums/resource-type';
import { ICommand } from './models/command';
import { CommandType } from './enums/command-type';

export const commandsMap = new Map<CommandType, ICommand>([
  [CommandType.Bloc, { fileName: 'fileName', resource: ResourceType.Bloc }],
  [CommandType.Start, { fileName: 'start', resource: ResourceType.Start }],
  [CommandType.Index, { fileName: 'fileName', resource: ResourceType.Index }],
]);
