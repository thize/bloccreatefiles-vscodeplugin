import * as path from 'path';
import { TemplateType } from './enums/template-type';
import { ResourceType } from './enums/resource-type';
import { IResource } from './models/resource';
import { OptionType } from './enums/option-type';
import { IConfig } from './models/config';

export const resources = new Map<ResourceType, IResource>([
  [
    ResourceType.Start,
    {
      locDirName: (loc, config: IConfig) =>
        !config.defaults.start.flat ? loc.fileName : loc.dirName,
      locDirPath: (loc, config) => path.join(loc.dirPath, loc.dirName),
      files: [
        { name: (config: IConfig) => 'bloc.dart', type: TemplateType.Bloc },
        { name: () => 'page.dart', type: TemplateType.startPage },
        { name: () => 'service.dart', type: TemplateType.Service },
        { name: () => 'index.dart', type: TemplateType.Index },
      ],
      createFolder: (config: IConfig) =>
        config && config.defaults && config.defaults.start
          ? !config.defaults.start.flat
          : false,
      options: [OptionType.Flat, OptionType.AppName],
    },
  ],[
    ResourceType.Bloc,
    {
      locDirName: (loc, config: IConfig) =>
        !config.defaults.bloc.flat ? loc.fileName : loc.dirName,
      locDirPath: (loc, config) => path.join(loc.dirPath, loc.dirName),
      files: [
        { name: (config: IConfig) => 'bloc.dart', type: TemplateType.Bloc },
        { name: () => 'page.dart', type: TemplateType.Page },
        { name: () => 'screen.dart', type: TemplateType.Screen },
        { name: () => 'service.dart', type: TemplateType.Service },
        { name: () => 'index.dart', type: TemplateType.Index },
      ],
      createFolder: (config: IConfig) =>
        config && config.defaults && config.defaults.bloc
          ? !config.defaults.bloc.flat
          : false,
      options: [OptionType.Flat, OptionType.AppName],
    },
  ],
  [
    ResourceType.Index,
    {
      files: [{ name: () => 'index.dart', type: TemplateType.Index }],
      createFolder: (config: IConfig) =>
        config && config.defaults && config.defaults.index ? !config.defaults.index.flat : false,
      options: [OptionType.Flat, OptionType.AppName],
    },
  ],
]);
