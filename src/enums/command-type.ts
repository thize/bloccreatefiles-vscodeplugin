export enum CommandType {
  Start = 'extension.addFlutter2Start',
  Bloc = 'extension.addFlutter2Bloc',
  Index = 'extension.addFlutter2zIndex',
}
