export enum TemplateType {
  Bloc = 'bloc.tmpl',
  Service = 'service.tmpl',
  Page = 'page.tmpl',
  Screen = 'screen.tmpl',
  Index = 'index.tmpl',
  startPage = 'startPage.tmpl',
}
