export interface IProject {
  version: string;
  name: string;
}

export interface IEnvironments {
  source: string;
  dev: string;
  prod: string;
}

export interface IApp {
  outDir?: string;
  assets?: string[];
  index?: string;
  main?: string;
  test?: string;
  tsconfig?: string;
  environments?: IEnvironments;
}

export interface IProperties {
  flat?: boolean;
  [k: string]: any;
}

export interface IDefaults {
  start?: IProperties;
  bloc?: IProperties;
  index?: IProperties;
}

export interface IConfig {
  defaults: IDefaults;
  appName: string;
}
