import { IConfig } from './../models/config';

export const config: IConfig = {
  appName: 'new_instagramm',
  defaults: {
    start: { flat: false },
    bloc: { flat: false },
  },
};
